package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

public class Greeter{
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);
        Random random = new Random();
        Utilities util = new Utilities();

        System.out.println("Enter an integer value:");
        int value = reader.nextInt();

        System.out.println("Here's your number doubled: " + util.doubleMe(value));
    }
}